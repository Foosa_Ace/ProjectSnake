import numpy as np
from scipy.linalg import solve
import time

start_time = time.time()

def gauss(A, b, x, n): 

    L = np.tril(A)     #using the numpy.tril() [1]
    U = np.triu(A)     #using the numpy.triu() [2]
    for i in range(n):
        x = np.dot(np.linalg.inv(L), b - np.dot(U, x)) #Gauss-Seidel formula
        print str(i).zfill(3),      #fill with 3 numbers in the left
        print(x)
    return x

'''___MAIN___'''

A = np.array([[3, 7, 13], [1, 5, 3], [12, 3, -5]])
b = [76, 28, 1]
x = [1, 1, 1]

n = 100

print "\t\t*****************\t\t"
print "\nThe Iterations are:\n"
print gauss(A, b, x, n)
print "\nSolving A, b:"
print solve(A, b)
print("\n Executed in %ss :)\n" % (time.time() - start_time))
print "\t\t*****************\t\t"


#[1]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.tril.html
#[2]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.triu.html
