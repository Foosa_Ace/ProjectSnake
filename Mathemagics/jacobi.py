import numpy as np
import time
from scipy.linalg import solve

start_time = time.time()

def jacobi(A, b, x, n):

    D = np.diag(A)               #[1]
    R = A - np.diagflat(D)       #[2]
    
    for i in range(n):
        x = (b - np.dot(R,x))/ D #[3] Jacobi formula
        print str(i).zfill(3),
        print(x)
    return x

'''___Main___'''

A = np.array([[3, 7, 13], [1, 5, 3], [12, 3, -5]])
b = [76, 28, 1]
x = [0, 0, 0]

n = 10000

print "\t\t*****************\t\t"
print "The Iterations are:\n"
print jacobi(A, b, x, n)
print"\nSolution:"
print solve(A, b)
print("\n--- %s seconds ---\n" % (time.time() - start_time))
print "\t\t*****************\t\t"


#[1]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.diag.html
#[2]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.diagflat.html
#[3]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.dot.html