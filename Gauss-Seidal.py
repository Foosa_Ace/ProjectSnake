import numpy as np
from scipy.linalg import solve

def gauss(A, b, x, n): 

    L = np.tril(A)     #using the numpy.tril() [1]
    U = np.triu(A)     #using the numpy.triu() [2]
    for i in range(n):
        x = np.dot(np.linalg.inv(L), b - np.dot(U, x)) #Gauss-Seidel formula
        print str(i).zfill(3),      #fill with 3 numbers in the left
        print(x)
    return x

'''___MAIN___'''

A = np.array([[3, 7, 13], [1, 5, 3], [12, 3, -5]])
b = [76, 28, 1]
x = [1, 1, 1]

n = 20

print "\nThe Iterations are:\n"
print gauss(A, b, x, n)
print "\nSolving A, b:\n"
print solve(A, b)

#[1]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.tril.html
#[2]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.triu.html